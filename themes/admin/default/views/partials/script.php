<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/mdb.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>summernote/summernote-lite.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/dropify.min.js"></script>
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>js/addons/datatables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>