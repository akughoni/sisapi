<!--Navbar -->
<nav class="mb-1 navbar fixed-top navbar-expand navbar-dark default-color p-3">
    <div class="container">
        <a class="navbar-brand mr-5 font-weight-bold" href="<?= base_url() ?>">SISAPI</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333" aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
            <ul class="navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('dashboard') ?>">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('peternak') ?>">Peternak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('kelurahan') ?>">Kelurahan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('jenis') ?>">Jenis Sapi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('kategori') ?>">Kategori Sapi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-3" href="<?= base_url('config') ?>">Web Config</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"> <?php $user = $this->ion_auth->user()->row();
                        echo $user->first_name; ?></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                        <a class="dropdown-item" href="<?= base_url('auth/') ?>">Manage Users</a>
                        <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar -->