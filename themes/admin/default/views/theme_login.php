<!DOCTYPE html>
<html>
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SI SAPI</title>
  <!-- base:css -->
  <?php echo (!empty($template['partials']['css'])) ? $template['partials']['css'] : ''; ?>
</head>

<body>
  <?php echo (!empty($template['partials']['content'])) ? $template['partials']['content'] : ''; ?>
  <?php echo (!empty($template['partials']['script'])) ? $template['partials']['script'] : ''; ?>
  <?php echo (!empty($template['partials']['script_page'])) ? $template['partials']['script_page'] : ''; ?>
</body>

</html>