<section class="section-footer">
	<div class="box-footer">
		<div class="content-footer">
			<div class="body-content">
				<div class="container">

					<div class="row">

						<div class="col-lg-4">
							<div>
								<h6>
									Terimakasih Kepada
								</h6>
								<img src="<?= base_url() ?>assets/img/img-logo.png" alt="" />
							</div>
							<div>
								<p class="mb-0">
									Universitas Islam Kadiri - Kediri
								</p>
								<p>
									Jl. Sersan Suharmaji No.38, Manisrenggo, Kec. Kota Kediri, Kediri, Jawa Timur 64128 <br>
									<a href="https://www.uniska-kediri.ac.id/">www.uniska-kediri.ac.id</a>
								</p>
							</div>
						</div>

						<div class="col-lg-4">
							<div>
								<h6>
									Deskripsi Sistem
								</h6>
								<p>
									Analisis status reproduksi sapi betina yang di kembangkan dalam bentuk Sistem Informasi ini berfungsi sebagai sistem recording data perkembangbiakan sapi di Kec. Ngadiluwih, Kediri.
								</p>
							</div>
						</div>

						<div class="col-lg-4">
							<div>
								<h6>
									Harapan dan Tujuan Kami
								</h6>
								<p>
									Dapat memudahkan masyarakat luas untuk memperoleh data perkembangbiakan sapi.
								</p>
							</div>
							<!-- <div>
								<h6>
									
								</h6>
								<p>
									
								</p>
							</div> -->
							<div>
								<h6>
									Sosial Media
								</h6>
								<ul>
									<li>
										<a href=""><i class="fab fa-facebook-square"></i></a>
									</li>
									<li>
										<a href=""><i class="fab fa-twitter-square"></i></a>
									</li>
									<li>
										<a href=""><i class="fab fa-instagram"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-content">
				<div class="container">
					<p>PROJECT SIS UNISKA | Universitas Islam Kadari - Kediri</p>
					<p><i class="fas fa-copyright"></i> 2019. All Rights Reserved</p>
				</div>
			</div>
		</div>
	</div>
</section>