<link href="https://fonts.googleapis.com/css?family=Lato|Montserrat&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="<?php echo theme_admin_locations(); ?>css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="<?php echo theme_admin_locations(); ?>css/mdb.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="<?php echo theme_front_locations(); ?>css/layout/form.css" rel="stylesheet">
<link href="<?php echo theme_front_locations(); ?>css/layout/general.css" rel="stylesheet">
<link href="<?php echo theme_front_locations(); ?>css/layout/layout.css" rel="stylesheet">
<link href="<?php echo theme_front_locations(); ?>css/layout/responsive.css" rel="stylesheet">
<link href="<?php echo theme_front_locations(); ?>css/layout/typography.css" rel="stylesheet">
<!-- <link href="<?php echo theme_admin_locations(); ?>css/dropify.css" rel="stylesheet"> -->
<!-- <link href="<?php echo theme_admin_locations(); ?>summernote/summernote-lite.css" rel="stylesheet"> -->
<!-- MDBootstrap Datatables  -->
<link href="<?php echo theme_admin_locations(); ?>css/addons/datatables.min.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<style>
  /* .content-body {
    margin-top: 6rem !important;
    min-height: 600px !important;
  }

  .navbar-nav .nav-item .nav-link:hover {
     font-weight: 500; 
    background-color: #3ec1b6;
  }

  #dataTable {
  width: 95% !important;
  } */

  body {}
</style>