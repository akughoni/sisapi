<!-- header section -->
<section class="section-navbar font-roboto">
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <div class="mobile-menu hidden-desktop">
                <ul class="left-menu">
                    <li>
                        <a class="navbar-brand" href="">SISAPI</a>
                    </li>
                </ul>
                <ul class="right-menu">
                    <li>
                        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-desktop-content">
							<span class="navbar-toggler-icon"></span>
						</button> -->
                    </li>
                    <li>
                    </li>
                </ul>
            </div>

            <div class="desktop-menu collapse navbar-collapse text-center" id="navbar-desktop-content">
                <!-- FOR NEXT DEVELPMENT -->
                <ul class="brand-menu">
                    <li>
                        <a class="navbar-brand hidden-mobile" href="">SISAPI</a>
                    </li>
                </ul>
                <ul class="main-menu">
                    <li class="nav-link">
                        <a class="nav-item" id="home-link" href="#home">Home</a>
                    </li>
                    <li class="nav-link">
                        <a class="nav-item" id="data-link" href="#data">Data</a>
                    </li>
                    <li class="nav-link">
                        <a class="nav-item" id="about-link" href="#about">Tentang</a>
                    </li>
                    </li>
                </ul>
                <!-- END FOR NEXT DEVELPMENT CONTENT-->
            </div>
        </div>
    </nav>
</section>
<!-- end header section -->