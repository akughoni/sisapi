<!DOCTYPE html>
<html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SISAPI - UNISKA</title>
  <!-- base:css -->
  <?php echo (!empty($template['partials']['css'])) ? $template['partials']['css'] : ''; ?>
</head>

<body>
  <div>
    <?php echo (!empty($template['partials']['header'])) ? $template['partials']['header'] : ''; ?>
    <?php echo (!empty($template['partials']['content'])) ? $template['partials']['content'] : ''; ?>
    <?php echo (!empty($template['partials']['footer'])) ? $template['partials']['footer'] : ''; ?>
  </div>
  <?php echo (!empty($template['partials']['script'])) ? $template['partials']['script'] : ''; ?>
  <?php echo (!empty($template['partials']['script_page'])) ? $template['partials']['script_page'] : ''; ?>
</body>

</html>