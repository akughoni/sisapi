<?php

class Base_admin extends MX_Controller
{
  protected $time_zone = 'Asia/Jakarta';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth', 'form_validation'));
    $this->load->helper(array('url', 'language', 'date'));

    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

    $this->lang->load('auth');
    if (!$this->ion_auth->logged_in()) {
      // redirect them to the login page
      redirect('auth/login', 'refresh');
    }
  }

  public function index()
  {

    if (!$this->ion_auth->logged_in()) {
      // redirect them to the login page
      redirect('mod_auth/login', 'refresh');
    } elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return show_error('You must be an administrator to view this page.');
    } else {
      // set the flash data error message if there is one
      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

      //list the users
      $this->data['users'] = $this->ion_auth->users()->result();
      foreach ($this->data['users'] as $k => $user) {
        $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
      }

      $this->_render_page('auth/index', $this->data);
    }
  }

  protected function display($tpl_content = 'partials/default.php', $data = [], $tpl_footer = 'partials/script_page')
  {
    $page_partials        = [
      'css'           => 'partials/css',
      'header'             => 'partials/header',
      'content'            => $tpl_content,
      'footer'             => 'partials/footer',
      'script'      => 'partials/script',
      'footer_script' => $tpl_footer
    ];
    $this->template_admin
      ->set_partial('css', $page_partials['css'])
      ->set_partial('header', $page_partials['header'])
      ->set_partial('content', $page_partials['content'], $data)
      ->set_partial('footer', $page_partials['footer'])
      ->set_partial('script', $page_partials['script'])
      ->set_partial('script_page', $page_partials['footer_script'])
      ->build('themes');
  }

  protected function get_date_now()
  {
    $date = now($this->time_zone);
    $date = unix_to_human($date, true, 'ID');
    return $date;
  }

  protected function get_unix_id()
  {
    return strtoupper(date('ymd') . uniqid());
  }
}
