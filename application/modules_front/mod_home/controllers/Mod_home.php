<?php

class Mod_home extends MX_Controller
{
  private $base_table = "acs_config";
  private $base_model = "home_model";
  private $primary_key = "id";
  private $field_get = array(
    'kode_res', 'nama','alamat', 'nama_kelurahan', 'peternak_umur', 'peternak_pendidikan', 'sapi_ke', 'kategori', 'umur', 'jenis', 'sc'
  );
  private $field_add = array(
    'peternak_id', 'jenis_sapi_id', 'sapi_kt_id', 'umur', 'sapi_ke'
  );


  public function __construct()
  {
    parent::__construct();
    $this->load->model($this->base_model);
    $this->load->helper(array(
      'url'
    ));
  }

  protected function display($tpl_content = 'partials/default.php', $data = [], $tpl_footer = 'partials/script_page')
  {
    $page_partials        = [
      'css'           => 'partials/css',
      'header'             => 'partials/header',
      'content'            => $tpl_content,
      'footer'             => 'partials/footer',
      'script'      => 'partials/script',
      'footer_script' => $tpl_footer
    ];
    $this->template_front
      ->set_partial('css', $page_partials['css'])
      ->set_partial('header', $page_partials['header'])
      ->set_partial('content', $page_partials['content'], $data)
      ->set_partial('footer', $page_partials['footer'])
      ->set_partial('script', $page_partials['script'])
      ->set_partial('script_page', $page_partials['footer_script'])
      ->build('themes');
  }

  public function index()
  {
    $data = [];
    $config = $this->{$this->base_model}->get_all_data($this->base_table);

    foreach($config as $col ){
      $data[$col['config_name']] = (empty($col['config_value']))? $col['config_img'] : $col['config_value'] ;
    }
    
    $this->display('index', $data, 'footer_script');
  }

  public function table()
  {
    $value = [];
    $data_json = [];
    $data = $this->{$this->base_model}->get_data_sapi($this->base_table);

    foreach ($data as $index => $col) {
      $value[] = $index + 1;
      foreach ($this->field_get as $get) {
        $value[] = $col[$get];
      }
      $data_button = [
        'id' => $col[$this->primary_key]
      ];
      // $button_action = $this->load->view('button', $data_button, true);
      // $value[] = $button_action;
      $data_json[$index] = $value;
      $value = [];
    }
    $result['data'] = $data_json;
    echo json_encode($result);
  }

  public function filtertable()
  {
    $list = $this->{$this->base_model}->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $index => $col) {
      $value = [];
      $no++;
      $value[] = $no;
      foreach ($this->field_get as $get) {
        $value[] = $col[$get];
      }

      $data[] = $value;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->{$this->base_model}->count_all(),
      "recordsFiltered" => $this->{$this->base_model}->count_filtered(),
      "jumlahSapi" => $this->{$this->base_model}->count_filtered(),
      "jumlahPeternak" => $this->{$this->base_model}->count_by_peternak(),
      "data" => $data,
    );
    //output dalam format JSON
    echo json_encode($output);
  }

  public function get_select_option()
  {
    $tabel = $this->input->post('tabel');
    // $this->load->model('admin_model');
    $result = $this->{$this->base_model}->get_all_data($tabel);

    echo json_encode($result);
  }
}