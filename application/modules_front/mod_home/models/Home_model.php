<?php
require_once APPPATH."models/Front_model.php";

class Home_model extends Front_model
{
  private $select = "SELECT a.id, b.id as kode_res , b.nama , b.alamat , c.nama_kelurahan , b.umur as peternak_umur, b.pendidikan as peternak_pendidikan, a.sapi_ke,
            kt_1.kategori , a.umur , d.jenis , a.sc ";
  private $from = 'FROM data_sapi a'; 
  private $join = " JOIN peternak b ON b.id = a.peternak_id
            JOIN kelurahan c ON b.kelurahan_id = c.id
            JOIN sapi_kt kt_1 ON kt_1.id = a.sapi_kt_id 
            JOIN jenis_sapi d ON d.id = a.jenis_sapi_id ";

  private $where_init = "WHERE 1=1 ";
  private $column_search = array('b.id', 'nama', 'alamat', 'nama_kelurahan', 'b.umur', 'b.pendidikan', 'sapi_ke', 'kategori', 'a.umur', 'jenis', 'sc'); 

  public function __construct()
  {
    parent::__construct();
  }

  private function _get_datatables_query()
  {
    $main_sql = '';
    $main_sql .= $this->select;
    $main_sql .= $this->from;
    $main_sql .= $this->join;
    $main_sql .= $this->where_init;
    
    foreach ($this->column_search as $i => $item) // looping awal
    {
      if (isset($_POST['id']) and $_POST['id'] != "") {
        $main_sql .= " AND c.id = '" . $_POST['id'] . "'";
      }

      if (isset($_POST['search']['value']) AND !empty($_POST['search']['value']))
      {
        if ($i === 0) // looping awal
        {
          $main_sql .= " AND ". $item . " LIKE '%" . $_POST['search']['value']. "%' ";
        } else {
          $main_sql .= " OR " . $item . " LIKE '%" . $_POST['search']['value'] . "%' ";
          // $this->db->or_like($item, $_POST['search']['value']);
        }
      }
    }
    return $main_sql;
  }

  function get_datatables()
  {
    $main_sql = $this->_get_datatables_query();
    // die($main_sql);

    if (isset($_POST['searchBy']) AND $_POST['searchBy'] != ""){
      if (isset($_POST['searchValue']) and $_POST['searchValue'] != "") {
        $main_sql .= " AND " . $_POST['searchBy'] . " LIKE '" . $_POST['searchValue'] . "%' ";        
      }
    }
    if (isset($_POST['length']) AND $_POST['length'] != -1){
      $main_sql .= " LIMIT ". $_POST['start'] .",".$_POST['length'];
    }
    
    $query = $this->db->query($main_sql);
    return $query->result_array();
  }

  function count_filtered()
  {
    $main_sql = $this->_get_datatables_query();
    $query = $this->db->query($main_sql);    
    return $query->num_rows();
  }

  public function count_all()
  {
    $main_sql = $this->_get_datatables_query();
    $query = $this->db->query($main_sql);
    return $query->num_rows();
  }

  public function count_by_peternak()
  {
    $main_sql = $this->_get_datatables_query();
    $main_sql .= "GROUP BY b.nama ";
    $query = $this->db->query($main_sql);
    return $query->num_rows();
  }
}