<!-- jumbotron section -->
<section id="home" class="section-jumbotron font-roboto">
  <div class="jumbotron" style="background-image: url('<?php echo base_url() . "assets/img/" . $bg_jumbotron; ?>')">
    <div class="overlay"></div>
    <div class="jumbotron-body">
      <div class="jumbotron-content">
        <div class="logo-content">
          <img src="<?php echo base_url() . "assets/img/" . $logo_kampus; ?>" alt="" />
        </div>
        <div class="header-content">
          <h1 class="display-4"><?= $jb_header ?></h1>
        </div>
        <div class="body-content">
          <p class=""><?= $jb_content ?></p>
        </div>
        <div class="footer-content">
          <a id="section-table" class="btn btn-info"><?= $jb_btn_text ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end header section -->
<div class="pt-5 py-3" id="data">
</div>
<section class="section-table" id="data">
  <!-- <div class="container-fluid"> -->
  <div class="header-box text-center container">
    <h1 class="font-lato"><?= $data_title ?></h1>
    <hr>
  </div>
  <div class="box-table">
    <div class="body-box">
      <form action="">
        <!-- search menu -->
        <div class="form-row justify-content-center">
          <div class="col-lg-3 col-sm-12">
            <div class="input-group input-group-sm">
              <select id="kelurahan" class="custom-select">
                <option value="" selected>Tampilkan Semua Desa</option>
              </select>
            </div>
          </div>
          <div class="col-lg-3 col-sm-12">
            <div class="input-group input-group-sm">
              <!-- <label for="searchBy" class="col-form-label mr-3">Filter </label> -->
              <select id="searchBy" name="searchBy" class="custom-select">
                <option value="" selected>Cari Berdasarkan...</option>
                <option>Nama</option>
                <option>Alamat</option>
                <option value="b.umur">Umur Peternak</option>
                <option value="b.pendidikan">Pend. Peternak</option>
                <option value="sapi_ke">Sapi-ke</option>
                <option>Kategori</option>
                <option>Jenis</option>
                <option value="a.umur">Umur Sapi</option>
                <option value="sc">S/C</option>
              </select>
            </div>
          </div>
          <div class="col-lg-3 col-sm-12">
            <div class="input-group input-group-sm">
              <label for="searchValue"></label>
              <input type="text" class="form-control" name="searchValue" id="searchValue" aria-describedby="helpId" placeholder="Cari disini...">
            </div>
          </div>
          <!-- <div class="col-lg-3 col-sm-12">
            <div class="mt-1">
              <p>Jumlah Sapi <span class=' ml-2 pl-1 pr-1'><b id="jmlSapi"></b></span>
                Jumlah Peternak <span class=' ml-2 pl-1 pr-1'><b id='jmlPeternak'></b></span>
                <p>
            </div>
          </div> -->
        </div>

        <div class="tablebox-body">
          <!-- tittle table -->
          <table id="dataTable" class="table table-hover table-responsive-sm">
            <thead>
              <tr>
                <th>No.</th>
                <th>Kode Resp.</th>
                <th>Pemilik</th>
                <th>Alamat</th>
                <th>Keluarahan</th>
                <th>Umur Pemilik</th>
                <th>Pend. Pemilik</th>
                <th>Sapi ke</th>
                <th>Kategori</th>
                <th>Usia (Bulan)</th>
                <th>Jenis</th>
                <th>S/C</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="12" style="text-align:right; font-size:1em;">
                  <span class='rounded pl-4 pr-4 p-1'>Jumlah Sapi <b class="ml-2" id="jmlSapi"></b></span>
                </th>
              </tr>
              <tr>
                <th colspan="12" style="text-align:right; font-size:1em;">
                  <span class='rounded pl-4 pr-4 p-1'>Jumlah Peternak <b class="ml-2" id='jmlPeternak'></b></span>
                </th>
              </tr>
            </tfoot>
          </table>
        </div>
      </form>
    </div>
    <!-- <div class="footer-box">
      <p>Total keseluruhan sapi <span class="badge badge-info">10010</span></p>
    </div> -->
    <!-- </div> -->
  </div>
</section>
<div class="mt-4" id="about">
</div>
<section id="about" class="section-about">
  <div class="container about-body">
    <div class="about-content">
      <div class="container">
        <div class="header-content">
          <div class="row">
            <div class="col-sm-12 text-center">
              <h1 class="font-lato text-black-50"><?= $about_title ?></h1>
              <hr>
            </div>
          </div>
        </div>
        <div class="body-content mt-2">
          <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-9">
              <div class="first-body-content">
                <div class="content-text-about font-montserrat text-center font-weight-lighter">
                  <p class="">
                    <?= $about_content ?>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-content"></div>
      </div>
    </div>
  </div>
</section>