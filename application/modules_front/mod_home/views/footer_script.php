<script>
  var base_url = "<?= base_url() ?>" + "mod_home/";
  var table = null;
  var peternak = null;
  $(document).ready(function() {
    // $('#dataTable thead tr').clone(true).appendTo('#dataTable thead');
    var table = $('#dataTable').DataTable({
      "paging": true,
      // "lengthMenu": [
      //   [10, 25, 50, 100, -1],
      //   [10, 25, 50, 100, "All"]
      // ],
      "pageLength": 10,
      "processing": true,
      "searchDelay": 500,
      "orderCellsTop": true,
      "serverSide": true,
      "dom": '<"toolbar">frtip', //custom toolbar
      "ordering": false,
      // "fixedColumns": true,
      "searching": false,
      "ajax": {
        "url": base_url + "filtertable",
        "type": "POST",
        "data": function(data) {
          var val = $('#kelurahan').val();
          var searchBy = $('#searchBy').val();
          var searchValue = $('#searchValue').val();
          data.id = val;
          data.searchBy = searchBy;
          data.searchValue = searchValue;
        },
        dataFilter: function(data) {
          var json = jQuery.parseJSON(data);
          console.log('datafilter');
          peternak = json.jumlahPeternak;
          $('.tablebox-body #dataTable tfoot tr th #jmlSapi').html(json.jumlahSapi);
          $('.tablebox-body #dataTable tfoot tr th #jmlPeternak').html(json.jumlahPeternak);
          return data; // return JSON string
        }
      },
      "initComplete": function(res) {
        // $('#peternak').val(res.json.jumlahPeternak);
        // console.log(res.json);
      },
      "language": {
        "info": "",
        "infoEmpty": "Data Kosong",
        "infoFiltered": "",
      },
    });

    // $("div.toolbar").html('<div class="col-lg-3 col-sm-12"><div class = "input-group input-group-sm"><select id = "kelurahan" class = "custom-select"><option value = "" selected> Tampilkan Semua Desa </option></select> </div> </div> <div class="col-lg-12 col-sm-12"><div class = "show-select-data"><p> Jumlah yang ditampilkan, sebanyak<span id="total " class = "font-weight-bold" > 12 </span>peternak</p></div></div>');
    // filter by selectbox keluarahn 
    $('#kelurahan').on('change', function(e) {
      table.draw();
    });
    $('#searchValue').on('keyup', function(e) {
      table.draw();
    });

    // set navbar
    $('.section-navbar #navbar').addClass('bg-transparetnt');
    // Navbar change color on scroll
    $(document).scroll(function() {
      var scroll = $(document).scrollTop();
      if (scroll > 500) {
        $('.section-navbar #navbar').removeClass('bg-transparetnt');
        $('.section-navbar #navbar').addClass('bg-varian1');
      } else {
        $('.section-navbar #navbar').addClass('bg-transparetnt');
      }
    });
    // scroll animate on klik navbar
    $("#home-link").click(function() {
      $('html, body').animate({
        scrollTop: $("#home").offset().top
      }, 1000);
    });
    $("#data-link").click(function() {
      $('html, body').animate({
        scrollTop: $("#data").offset().top
      }, 1000);
    });
    $("#section-table").click(function() {
      $('html, body').animate({
        scrollTop: $("#data").offset().top
      }, 1000);
    });
    $("#about-link").click(function() {
      $('html, body').animate({
        scrollTop: $("#about").offset().top
      }, 1000);
    });
    // set Data Option Kelurahan
    $.ajax({
      type: "POST",
      url: base_url + 'get_select_option',
      data: {
        tabel: 'kelurahan'
      },
      dataType: "JSON",
      success: function(res) {
        res.forEach(value => {
          // console.log(el);
          var option = "<option value='" + value.id + "'>" + value.nama_kelurahan + "</option>";
          $('#kelurahan').append(option);
          // $('#form_edit #sapi_kt_id').append(option);
        });
      }
    });


  });
</script>