<?php

class Base_model extends CI_Model
{
  protected $sql = "SELECT 	a.peternak_id , b.nama , b.alamat, b.umur as peternak_umur , b.pendidikan as peternak_pend,
                        a.sapi_ke, e.kategori as katergori_sapi, a.umur as umur_sapi, d.jenis as jenis_sapi, a.sc
                    FROM data_sapi a
                    JOIN peternak b ON a.peternak_id = b.id
                    JOIN kelurahan c ON b.kelurahan_id = c.id
                    JOIN jenis_sapi d ON a.jenis_sapi_id = d.id
                    JOIN sapi_kt e on a.sapi_kt_id = e.id
                    WHERE 1=1";

  public function get_all_data($table = null)
  {
    $query = $this->db->get($table);
    if (!empty($query)) {
      return $query->result_array();
    } else {
      return array();
    }
  }

  public function get_data_by_id($data= [], $table=null)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where('id', $data['id']);
    $query = $this->db->get();
    
    if(!empty($query)){
      return $query->row();
    }
  }

  public function add_data($data = [], $table = null)
  {
    return $this->db->insert($table, $data);
  }

  public function update_data_by_id($id = null, $data = [] , $table = null)
  {
    $this->db->where('id', $id);
    return $this->db->update($table, $data);
  }

  public function delete_data_by($data= [], $table= null) 
  {
    $this->db->where('id', $data['id']);
    return $this->db->delete($table, $data);
  }

  public function get_data_by($table = null, $by = null,  $data= null)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($by, $data);
    $query = $this->db->get();

    if (!empty($query)) {
      return $query->row();
    }
  }

  public function get_count_data_by($table = null, $field = null, $data = null)
  {
    $sql = "SELECT COUNT(?) as total FROM data_sapi  WHERE peternak_id = ?";
    $query = $this->db->query($sql, array($field, $data ));
    if(!empty($query)){
      return $query->row();
    }else {
      return array();
    }
  }
  public function get_data_peternak()
  {
    $sql = "SELECT a.id , a.nama, a.alamat , b.nama_kelurahan , a.umur, a.pendidikan
              FROM peternak a
              JOIN kelurahan b ON a.kelurahan_id = b.id
              ORDER by a.ctd DESC";
    $query = $this->db->query($sql);
    if(!empty($query)){
      return $query->result_array();
    }else{
      return array();
    }
  }

  public function get_data_sapi()
  {
    $sql = "SELECT a.id, b.id as kode_res , b.nama , b.alamat , c.nama_kelurahan , b.umur as peternak_umur, b.pendidikan as peternak_pendidikan, a.sapi_ke,
            kt_1.kategori , a.umur , d.jenis , a.sc
            FROM data_sapi a
            JOIN peternak b ON b.id = a.peternak_id
            JOIN kelurahan c ON b.kelurahan_id = c.id
            JOIN sapi_kt kt_1 ON kt_1.id = a.sapi_kt_id 
            JOIN jenis_sapi d ON d.id = a.jenis_sapi_id
            WHERE 1=1";
    $query = $this->db->query($sql);
    if (!empty($query)) {
      return $query->result_array();
    } else {
      return array();
    }
  }

  // public function get_data_sapi_by_kelurahan_id($data = null )
  // {
  //   $sql = "SELECT a.id, b.id as kode_res , b.nama , b.alamat , c.nama_kelurahan , b.umur as peternak_umur, b.pendidikan as peternak_pendidikan, a.sapi_ke,
  //           kt_1.kategori , a.umur , d.jenis , a.sc
  //           FROM data_sapi a
  //           JOIN peternak b ON b.id = a.peternak_id
  //           JOIN kelurahan c ON b.kelurahan_id = c.id
  //           JOIN sapi_kt kt_1 ON kt_1.id = a.sapi_kt_id 
  //           JOIN jenis_sapi d ON d.id = a.jenis_sapi_id
  //           WHERE 1=1 ";
  //   if($data == ''){
  //     $query = $this->db->query($sql);
  //   }else{
  //     $sql .= " AND c.id = '?'";
  //     $query = $this->db->query($sql, array($data));
  //   }

  //   if (!empty($query)) {
  //     return $query->result_array();
  //   } else {
  //     return array();
  //   }
  // }
}
