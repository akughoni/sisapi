<?php
use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/Base_admin.php');

class Mod_config extends Base_admin
{
	protected $base_model = "config_model";
	protected $base_table = "acs_config";

	protected $page = [
		'index' => 'index',
		'insert' => 'insert_form',
		'edit' => 'edit_form',
		'footer_script' => 'footer_script',
		'footer_editor' => 'footer_editor',
	];
	private $title_page = "WEB CONFIG";
	private $primary_key = "id";
	private $field_get = array(
		'config_group', 'config_name', 'config_value', 'config_desc', 'config_img', 'config_location'
	);
	private $field_add = array(
		'config_group', 'config_name', 'config_value', 'config_desc'
	);
	private $field_edit = array(
		'config_group', 'config_name', 'config_img', 'config_location'
	);

	function __construct()
	{
		parent::__construct();
		$this->load->model($this->base_model);
		$this->load->library(array('upload', 'form_validation'));
		$this->load->helper(array('file','url','form'));
	}

	public function index()
	{
		$data['title_page'] = $this->title_page;
		$this->display($this->page['index'], $data, $this->page['footer_script']);
	}

	public function table()
	{
		$value = [];
		$data_json = [];
		$data = $this->{$this->base_model}->get_all_data($this->base_table);

		foreach ($data as $index => $col) {
			$value[] = $index + 1;
			foreach ($this->field_get as $get) {
				$value[] = $col[$get];
			}
			$data_button = [
				'id' => $col[$this->primary_key]
			];
			$button_action = $this->load->view('button', $data_button, true);
			$value[] = $button_action;
			$data_json[$index] = $value;
			$value = [];
		}
		$result['data'] = $data_json;
		echo json_encode($result);
	}

	public function add()
	{
		$data['title_page'] = "FORM INPUT WEB CONFIG";
		$this->display($this->page['insert'], $data, $this->page['footer_editor']);
	}

	public function stroe_add()
	{
		$now = $this->get_date_now(); //get date now
		$data = [];

		$upload = $this->upload_image();
		if ($upload['status']) {
			//set data from post to data by field properties
			foreach ($this->field_add as $value) {
				$data[$value] = $this->input->post($value);
			}
			$data['config_img'] = $upload['file_name'];
			$data['config_location'] = $upload['location'];
			$data['id'] = $this->get_unix_id();
			$data['ctd'] = $now;
			$data['mdd'] = $now;
			$result = $this->{$this->base_model}->add_data($data, $this->base_table);
			redirect('config');
		}else{
			redirect('config/add');
		}
	}

	public function upload_image()
	{
		$data = [];
		$data['status'] = false;
		$config['upload_path'] = './assets/img/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		// $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		var_dump($_FILES);
		$this->upload->initialize($config);
		if (!empty($_FILES['config_img']['name'])) {
			if ($this->upload->do_upload('config_img')) {
				$result = $this->upload->data();
				$data['file_name'] = $result['file_name'];
				$data['location'] = "assets/img/";
				$data['status'] = true;
				$data['message'] = "Upload Berhasil";
			} else {
				$data['error'] = array('error' => $this->upload->display_errors());
				$data['message'] =  "Gambar Gagal Upload. Gambar harus bertipe gif|jpg|png|jpeg|bmp";
			}
		} else {
			$data['status'] = true;
			$data['message'] =  "Gagal, gambar belum di pilih";
		}

		return $data;
	}

	public function edit($id)
	{
		// $param['id'] = $this->input->post('id');
		$param['id'] = $id;
		$result = $this->{$this->base_model}->get_data_by_id($param, $this->base_table);
		$data['result'] = (array) $result;
		// echo json_encode($result);
		$data['title_page'] = "FORM EDIT CONFIG";
		// set form input by field_get
		foreach($this->field_edit as $col){
			$data[$col] = array(
				'name'  => $col,
				'id'    => $col,
				'type'  => 'text',
				'value' => $this->form_validation->set_value($col, $result->{$col}),
				'class' => 'form-control'
			);
		}
		// set hidden form-input
		$data['config_value'] = array(
			'name'  => 'config_value',
			'id'    => 'config_value',
			'type'  => 'textarea',
			'value' => $this->form_validation->set_value('config_value', $result->config_value),
			'class' => 'form-control'
		);
		$data['config_desc'] = array(
			'name'  => 'config_desc',
			'id'    => 'config_desc',
			'value' => $this->form_validation->set_value('config_desc', $result->config_desc),
			'class' => 'form-control',
			'rows' => '3'
		);
		$data['id'] = array(
			'id' => $this->form_validation->set_value('id', $result->id),
		);

		$this->display($this->page['edit'], $data, $this->page['footer_editor']);
	}

	public function store_edit()
	{
		$id = $this->input->post($this->primary_key);
		$now = $this->get_date_now(); //get date now
		$data = [];
		//set data from post to data by field properties
		$upload = $this->upload_image();
		if ($upload['status']) {
			//set data from post to data by field properties
			foreach ($this->field_add as $value) {
				$data[$value] = $this->input->post($value);
			}
			if (!empty($upload['file_name'])) {	
				$data['config_img'] = $upload['file_name'];
				$data['config_location'] = $upload['location'];
			}
			$data['mdd'] = $now;
			$this->delete_img($id);
			$result = $this->{$this->base_model}->update_data_by_id($id, $data, $this->base_table);
			redirect('config');
		}else{
			redirect('config/edit');
		}
		// $data['mdd'] = $now;
		// $id = $this->input->post($this->primary_key);
		// $result = $this->{$this->base_model}->update_data_by_id($id, $data, $this->base_table);
		return $result;
	}

	public function delete_img($id)
	{
		$data['id'] = $id;
		//use if delete with file
		$get_data = $this->{$this->base_model}->get_data_by_id($data, $this->base_table);
		//cek apakah data berisi file atau tidak
		if ($get_data->config_img != null) {
			$path = $get_data->config_location . "/" . $get_data->config_img;
			if (unlink($path)) { //hapus file di storage
				$result = true;
			} else {
				$result = false;
			}
		}else{
			$result = null;
		}

		return $result;
	}

	public function delete()
	{
		$data['id'] = $this->input->post('data_id');
		//use if delete with file
		$get_data = $this->{$this->base_model}->get_data_by_id($data, $this->base_table);
		//cek apakah data berisi file atau tidak
		if($get_data->config_img != null){
			$path = $get_data->config_location ."/".$get_data->config_img;
			if(unlink($path)){ //hapus file di storage
				$result = $this->{$this->base_model}->delete_data_by($data, $this->base_table);
			}else{
				$result = false;
			}
		}else{
			$result = $this->{$this->base_model}->delete_data_by($data, $this->base_table);
		}
		echo json_encode($result);
	}

	public function get_select_option()
	{
		$tabel = $this->input->post('tabel');
		$this->load->model('admin_model');
		$result = $this->admin_model->get_all_data($tabel);

		echo json_encode($result);
	}

	public function get_data_by_nama_peternak()
	{
		$table_get_data = $this->input->post('table');
		$table_get_count = 'data_sapi';
		$param['nama'] = $this->input->post('nama');
		$by_peternak = 'nama';
		$by_sapi = 'id';
		$result['peternak'] = $this->{$this->base_model}->get_data_by($table_get_data, $by_peternak, $param['nama']);
		$param['id'] = $result['peternak']->id;
		$result['sapi'] = $this->{$this->base_model}->get_count_data_by($table_get_count, $by_sapi, $param['id']);

		echo json_encode($result);
	}

	public function get_data_by_id_peternak()
	{
		$table = $this->input->post('table');
		$param['id'] = $this->input->post('id');
		$by_peternak = 'id';
		$result = $this->{$this->base_model}->get_data_by($table, $by_peternak, $param['id']);
		echo json_encode($result);
	}
}
