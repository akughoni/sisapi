<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section class="col-12">
  <div class="row text-center">
    <div class="col-12 col-md-12">
      <h3></h3>
    </div>
  </div>
  <hr>
</section>
<section>
  <form method="post" id="form_add" action="<?= base_url('config/store_edit'); ?>" enctype="multipart/form-data">
    <div class="row justify-content-center mb-5">
      <div class="col-12 col-md-8">
        <div class="form-group">
          <label for="config_group">Group</label>
          <!-- <input type="text" class="form-control" name="config_group" id="config_group" aria-describedby="helpId"> -->
          <?php echo form_input($config_group); ?>
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="config_name">Name</label>
          <!-- <input type="text" class="form-control" name="config_name" id="config_name" aria-describedby="helpId"> -->
          <?php echo form_input($config_name); ?>
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="config_desc">Desc</label>
          <!-- <input type="text" class="form-control" name="config_desc" id="config_desc" aria-describedby="helpId"> -->
          <?php echo form_textarea($config_desc); ?>
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="config_value">Value</label>
          <!-- <textarea name="config_value" id="config_value"></textarea> -->
          <?php echo form_textarea($config_value); ?>
          <small id="helpId" class="form-text text-muted"></small>
        </div>
      </div>
      <?php echo form_hidden($id); ?>
      <div class="col-12 col-md-4">
        <div class="form-group">
          <label for="config_img">Image</label>
          <input type="file" class="form-control-file" name="config_img" id="config_img" data-default-file="<?= base_url().$result['config_location'].$result['config_img'] ?>">
          <small id="fileHelpId" class="form-text text-muted"></small>
        </div>
        <div class="col-12">
          <hr>
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="<?= base_url('config') ?>" form="form_add" class="btn btn-outline-primary">Cancel</a>
        </div>
      </div>
    </div>
  </form>
</section>