<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section>
  <div class="row text-right">
    <div class="col-12 col-md-12">
      <a class="btn btn-md btn-primary btn-icon-text" id="btn_add" href="<?= base_url('config/add') ?>">
        <i class="fas fa-plus"></i> Add
      </a>
      <button type="button" id="btn_rld" class="btn btn-md btn-success btn-icon-text">
        <i class="fas fa-sync"></i> Refresh
      </button>
    </div>
  </div>
  <hr>
</section>
<section>
  <div class="table">
    <table class="table table-hover bg-aqua text-center" id="dataTable">
      <thead>
        <tr>
          <th class="text-center">No.</th>
          <th class="text-center">Group</th>
          <th class="text-center">Name</th>
          <th class="text-center">Value</th>
          <th class="text-center">Desc</th>
          <th class="text-center">Img</th>
          <th class="text-center">Img. Location</th>
          <th class="text-center">Actions</th>
        </tr>
      </thead>
    </table>
  </div>
</section>