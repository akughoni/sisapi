<script type="text/javascript">
  var base_url = "<?= base_url() ?>" + "mod_config/";
  var table = null;
  var nama = [];
  $(document).ready(function() {
    $('#input_sc').hide();

    var table = $('#dataTable').DataTable({
      // "autoWidth": true,
      "processing": true,
      "searchDelay": 500,
      "orderCellsTop": true,
      // "fixedHeader": true,
      // "serverSide": true,
      "ordering": true,
      "ajax": {
        "url": base_url + "table",
        "type": "POST"
      },
      "language": {
        "info": "Menampilkan _START_ s/d _END_ dari _TOTAL_ Data",
        "infoEmpty": "Data Kosong",
        "infoFiltered": "(Keseluruhan _MAX_ data)",
      }
      // 'columnDefs': 
    });
    // Get Data Option Kategori Sapi
    // $.ajax({
    //   type: "POST",
    //   url: base_url + 'get_select_option',
    //   data: {
    //     tabel: 'sapi_kt'
    //   },
    //   dataType: "JSON",
    //   success: function(res) {
    //     res.forEach(value => {
    //       // console.log(el);
    //       var option = "<option value='" + value.id + "'>" + value.kategori + "</option>";
    //       $('#form_add #sapi_kt_id').append(option);
    //       $('#form_edit #sapi_kt_id').append(option);
    //     });
    //   }
    // });

    // // Get Data Option Jenis Sapi
    // $.ajax({
    //   type: "POST",
    //   url: base_url + 'get_select_option',
    //   data: {
    //     tabel: 'jenis_sapi'
    //   },
    //   dataType: "JSON",
    //   success: function(res) {
    //     res.forEach(value => {
    //       // console.log(el);
    //       var option = "<option value='" + value.id + "'>" + value.jenis + "</option>";
    //       $('#form_add #jenis_sapi_id').append(option);
    //       $('#form_edit #jenis_sapi_id').append(option);
    //     });
    //   }
    // });
    // // Get Data Peternak Autocomplete
    // $.ajax({
    //   type: "POST",
    //   url: base_url + 'get_select_option',
    //   data: {
    //     tabel: 'peternak'
    //   },
    //   dataType: "JSON",
    //   success: function(res) {
    //     res.forEach(value => {
    //       nama.push(value.nama);
    //     });
    //   }
    // });
    // // Set Autocomplete for name
    // $("#nama").autocomplete({
    //   source: nama
    // });
    //Set Autocomplete for modal edit name
    // $("#formEditModal .modal-body #form_edit #nama").autocomplete({
    //   source: nama
    // });
    // Get data koresponden and sapi_ke
    $('#nama').change(function(e) {
      var param = $('#nama').val();
      $.ajax({
        type: "POST",
        url: base_url + 'get_data_by_nama_peternak',
        data: {
          table: 'peternak',
          nama: param
        },
        dataType: "JSON",
        success: function(res) {
          // console.log(res);
          $('#peternak_id').val(res.peternak.id);
          var sapi_ke = parseInt(res.sapi.total) + 1;
          $('#sapi_ke').val(sapi_ke);
        }
      });
      e.preventDefault();

    });
    // Add form SC if sapi_kt == betina
    $('#form_add #sapi_kt_id').change(function() {
      var sapi_kt = $('#sapi_kt_id option:selected').text()
      if (sapi_kt == 'Betina') {
        $('#input_sc').show();
      } else {
        $('#input_sc').hide();
      }
    });
    // Add form SC if sapi_kt == betina
    $('#form_edit #sapi_kt_id').change(function() {
      var sapi_kt = $('#sapi_kt_id option:selected').text()
      if (sapi_kt == 'Betina') {
        $('#input_sc').show();
      } else {
        $('#input_sc').hide();
      }
    });
    //reload datatables
    $('#btn_rld').click(function(e) {
      table.ajax.reload();
    });
    // ajax for delete data by btn-action delete
    $('#dataTable tbody').on('click', 'td #btn_del', function() {
      var id = $(this).attr('data-id');
      swal({
          title: "Hapus data",
          text: "Apakah anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: "POST",
              url: base_url + "delete",
              data: {
                data_id: id
              },
              dataType: "json",
              success: function(res) {
                if (res == true) {
                  swal('berhasil', {
                    icon: "success",
                  });
                  table.ajax.reload();
                } else {
                  swal('Gagal', {
                    icon: "error",
                  });
                }
              }
            });
          } else {
            swal("Hapus data dibatalkan!");
          }
        });
    });

    //Get data for form modal edit data
    $('#dataTable tbody').on('click', 'td #btn_edit', function() {
      var id = $(this).attr('data-id');
      $.ajax({
        type: "POST",
        url: base_url + "edit",
        data: {
          id: id
        },
        dataType: "JSON",
        success: function(res) {
          set_name_value(res.peternak_id);
          $('#formEditModal #id').val(res.id);
          $('#formEditModal #nama').val(res.nama);
          $('#formEditModal #sapi_ke').val(res.sapi_ke);
          $('#formEditModal #sapi_kt_id').val(res.sapi_kt_id);
          var sapi_kt = $('#sapi_kt_id option:selected').text()
          if (sapi_kt == 'Betina') {
            $('#input_sc').show();
          } else {
            $('#input_sc').hide();
          }
          $('#formEditModal #jenis_sapi_id').val(res.jenis_sapi_id);
          $('#formEditModal #umur').val(res.umur);
        }
      });
    });

    function set_name_value(id) {
      $.ajax({
        type: "POST",
        url: base_url + "get_data_by_id_peternak",
        data: {
          table: 'peternak',
          id: id
        },
        dataType: "JSON",
        success: function(res) {
          $('#formEditModal #nama').val(res.nama);
          $('#formEditModal #peternak_id').val(res.id);
        }
      });
    }

    //changes data proses after edit data on save
    $('#formEditModal #btn_save_edit').click(function(e) {
      var data = $('#form_edit').serialize();
      $.ajax({
        type: "POST",
        url: base_url + "store_edit",
        data: data,
        success: function(res) {
          table.ajax.reload();
          $('.modal-footer #btn_close').click();
        }
      });
    });

    // $('#btn_add').on('click', set_option());
    //Delete Data by Button on action

    //Add data on modal form
    // $('#btn_save').click(function(e) {
    //   var data = $('#form_add').serialize();
    //   console.log(data);
    //   $.ajax({
    //     type: "POST",
    //     url: base_url + "add",
    //     data: data,
    //     success: function(response) {
    //       table.ajax.reload();
    //       $('#btn_close').click();
    //     }
    //   });
    // });


  });
</script>