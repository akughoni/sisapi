<div class="modal fade" id="formEditModal" tabindex="-1" role="dialog" aria-labelledby="formEditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header default-color">
        <h5 class="modal-title text-white" id="formEditModalTitle">Edit Data Sapi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_edit">
          <input type="hidden" class="form-control" name="id" id="id" aria-describedby="helpId" placeholder="id">
          <div class="form-group">
            <label for="nama">Nama Peternak</label>
            <input type="text" class="form-control" name="nama" id="nama" aria-describedby="helpId" placeholder="Nama">
            <small id="helpId" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label for="peternak_id">Kode Responden</label>
            <input type="text" class="form-control" name="peternak_id" id="peternak_id" aria-describedby="helpId" placeholder="Kode Responde">
            <small id="helpId" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label for="sapi_ke">Sapi Ke-</label>
            <input type="text" class="form-control" name="sapi_ke" id="sapi_ke" aria-describedby="helpId" placeholder="Sapi Ke">
            <small id="helpId" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label for="sapi_kt_id">Kategori Sapi</label>
            <select class="custom-select" name="sapi_kt_id" id="sapi_kt_id">
              <option selected>Select one</option>
            </select>
          </div>
          <div class="form-group">
            <label for="jenis_sapi_id">Jenis Sapi</label>
            <select class="custom-select" name="jenis_sapi_id" id="jenis_sapi_id">
              <option selected>Select one</option>
            </select>
          </div>
          <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" aria-describedby="helpId" placeholder="Umur Sapi">
            <small id="helpId" class="form-text text-muted"></small>
          </div>
          <!-- Jika sapi betina, maka muncul S/C -->
          <div id="input_sc">
            <div class="form-group">
              <label for="sc">S/C</label>
              <input type="text" class="form-control" name="sc" id="sc" aria-describedby="helpId" placeholder="S/C Sapi">
              <small id="helpId" class="form-text text-muted"></small>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_close" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" id="btn_save_edit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>