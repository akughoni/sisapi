<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section class="col-12">
  <div class="row text-center">
    <div class="col-12 col-md-12">
      <h3>FORM INPUT DATA SAPI</h3>
    </div>
  </div>
  <hr>
</section>
<section>
  <div class="row justify-content-center mb-5">
    <div class="col-8">
      <form method="post" id="form_add" action="<?= base_url('dashboard/stroe_add'); ?>">
        <div class="form-group">
          <label for="nama">Nama Peternak</label>
          <input type="text" class="form-control" name="nama" id="nama" aria-describedby="helpId" placeholder="Nama">
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="peternak_id">Kode Responden</label>
          <input type="text" class="form-control" name="peternak_id" id="peternak_id" aria-describedby="helpId" placeholder="Kode Responde">
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="sapi_ke">Sapi Ke-</label>
          <input type="text" class="form-control" name="sapi_ke" id="sapi_ke" aria-describedby="helpId" placeholder="Sapi Ke">
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
          <label for="sapi_kt_id">Kategori Sapi</label>
          <select class="custom-select" name="sapi_kt_id" id="sapi_kt_id">
            <option selected>Select one</option>
          </select>
        </div>
        <div class="form-group">
          <label for="jenis_sapi_id">Jenis Sapi</label>
          <select class="custom-select" name="jenis_sapi_id" id="jenis_sapi_id">
            <option selected>Select one</option>
          </select>
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" name="umur" id="umur" aria-describedby="helpId" placeholder="Umur Sapi">
          <small id="helpId" class="form-text text-muted"></small>
        </div>
        <!-- Jika sapi betina, maka muncul S/C -->
        <div id="input_sc">
            <div class="form-group">
              <label for="sc">S/C</label>
              <input type="text" class="form-control" name="sc" id="sc" aria-describedby="helpId" placeholder="S/C Sapi">
              <small id="helpId" class="form-text text-muted"></small>
            </div>
        </div>
      </form>
    </div>
    <div class="col-8">
      <hr>
      <button type="submit" form="form_add" class="btn btn-primary">Submit</button>
      <button type="reset" form="form_add" class="btn btn-danger">Reset</button>
    </div>
  </div>
</section>