<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section>
  <div class="row text-right">
    <div class="col-12 col-md-12">
      <a class="btn btn-md btn-primary btn-icon-text" id="btn_add" href="<?= base_url('dashboard/add') ?>">
        <i class="fas fa-plus"></i> Add
      </a>
      <button type="button" id="btn_rld" class="btn btn-md btn-success btn-icon-text">
        <i class="fas fa-sync"></i> Refresh
      </button>
    </div>
  </div>
  <hr>
</section>
<section>
  <div class="table">
    <table class="table table-hover bg-aqua text-center" id="dataTable">
      <thead>
        <tr>
          <th class="text-center">No.</th>
          <th class="text-center">Kode Resp.</th>
          <th class="text-center">Pemilik</th>
          <th class="text-center">Kelurahan</th>
          <th class="text-center">Umur Pemilik</th>
          <th class="text-center">Pend. Pemilik</th>
          <th class="text-center">Sapi ke</th>
          <th class="text-center">Kategori</th>
          <th class="text-center">Umur</th>
          <th class="text-center">Jenis</th>
          <th class="text-center">S/C</th>
          <th class="text-center">Actions</th>
        </tr>
      </thead>
    </table>
  </div>
</section>
<?php $this->load->view('modal_edit'); ?>