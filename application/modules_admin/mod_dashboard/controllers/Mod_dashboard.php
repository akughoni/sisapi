<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/Base_admin.php');

class Mod_dashboard extends Base_admin
{
	protected $base_model = "dashboard_model";
	protected $base_table = "data_sapi";

	protected $page = [
		'index' => 'index',
		'insert' => 'insert_form',
		'edit' => 'edit_form',
		'footer_script' => 'footer_script',
		'footer_script_edit' => 'footer_script_edit',
	];
	private $title_page = "DASHBOARD";
	private $primary_key = "id";
	private $field_get = array(
		'kode_res', 'nama', 'nama_kelurahan', 'peternak_umur', 'peternak_pendidikan' , 'sapi_ke', 'kategori', 'umur', 'jenis', 'sc'
	);
	private $field_add = array(
		'peternak_id', 'jenis_sapi_id', 'sapi_kt_id', 'umur','sapi_ke'
	);

	function __construct()
	{
		parent::__construct();
		$this->load->model($this->base_model);
	}

	public function index()
	{
		$data['title_page'] = $this->title_page;
		$this->display($this->page['index'], $data, $this->page['footer_script']);
	}

	public function table()
	{
		$value = [];
		$data_json = [];
		$data = $this->{$this->base_model}->get_data_sapi($this->base_table);

		foreach ($data as $index => $col) {
			$value[] = $index + 1;
			foreach ($this->field_get as $get) {				
				$value[] = $col[$get];
			}
			$data_button = [
				'id' => $col[$this->primary_key]
			];
			$button_action = $this->load->view('button', $data_button, true);
			$value[] = $button_action;
			$data_json[$index] = $value;
			$value = [];
		}
		$result['data'] = $data_json;
		echo json_encode($result);
	}

	public function add()
	{
		$data['title_page'] = $this->title_page;
		$this->display($this->page['insert'], $data, $this->page['footer_script']);
	}

	public function stroe_add()
	{
		$now = $this->get_date_now(); //get date now
		$data = [];

		//set data from post to data by field properties
		foreach($this->field_add as $value){
			$data[$value] = $this->input->post($value);
		}
		$data['sc'] = (!empty($this->input->post('sc'))) ? $this->input->post('sc') : null ;
		$data['id'] = $this->get_unix_id();
		$data['ctd'] = $now;
		$data['mdd'] = $now;
		$result = $this->{$this->base_model}->add_data($data, $this->base_table);
		redirect('dashboard');
	}

	public function edit()
	{
		$data['id'] = $this->input->post('id');
		$result = $this->{$this->base_model}->get_data_by_id($data, $this->base_table);
		echo json_encode($result);
		// $data['title_page'] = $this->title_page;
		// $this->display($this->page['edit'], $data, $this->page['footer_script_edit']);
	}

	public function store_edit()
	{
		$now = $this->get_date_now(); //get date now
		$data = [];
		//set data from post to data by field properties
		foreach ($this->field_add as $value) {
			$data[$value] = $this->input->post($value);
		}
		$data['mdd'] = $now;
		$id = $this->input->post($this->primary_key);
		$result = $this->{$this->base_model}->update_data_by_id($id, $data, $this->base_table);
		return $result;
	}

	public function delete()
	{
		$data['id'] = $this->input->post('data_id');
		$result = $this->{$this->base_model}->delete_data_by($data, $this->base_table);
		echo json_encode($result);
	}

	public function get_select_option()
	{
		$tabel = $this->input->post('tabel');
		$this->load->model('admin_model');
		$result = $this->admin_model->get_all_data($tabel);

		echo json_encode($result);
	}

	public function get_data_by_nama_peternak()
	{
		$table_get_data = $this->input->post('table');
		$table_get_count = 'data_sapi';
		$param['nama'] = $this->input->post('nama');
		$by_peternak = 'nama';
		$by_sapi = 'id';
		$result['peternak'] = $this->{$this->base_model}->get_data_by($table_get_data, $by_peternak , $param['nama']);
		$param['id'] = $result['peternak']->id;
		$result['sapi'] = $this->{$this->base_model}->get_count_data_by($table_get_count, $by_sapi , $param['id'] );

		echo json_encode($result);
	}

	public function get_data_by_id_peternak()
	{
		$table = $this->input->post('table');
		$param['id'] = $this->input->post('id');
		$by_peternak = 'id';
		$result = $this->{$this->base_model}->get_data_by($table, $by_peternak , $param['id']);
		echo json_encode($result);
	}
}
