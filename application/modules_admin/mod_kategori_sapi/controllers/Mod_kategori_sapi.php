<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/Base_admin.php');

class Mod_kategori_sapi extends Base_admin
{
	protected $base_model = "kategori_sapi_model";
	protected $base_table = "sapi_kt";

	protected $page = [
		'index' => 'index',
		'footer_script' => 'footer_script',
	];
	private $title_page = "KATEGORI SAPI";

	function __construct()
	{
		parent::__construct();
		$this->load->model($this->base_model);
	}

	public function index()
	{
		$data['title_page'] = $this->title_page;
		$this->display($this->page['index'], $data, $this->page['footer_script']);
	}

	public function table()
	{
		$value = [];
		$data_json = [];
		$data = $this->{$this->base_model}->get_all_data($this->base_table);
		foreach ($data as $index => $col) {
			$value[] = $index + 1;
			$value[] = $col['kategori'];
			$data_button = [
				'id' => $col['id']
			];
			$button_action = $this->load->view('button', $data_button, true);
			$value[] = $button_action;
			$data_json[$index] = $value;
			$value = [];
		}
		$result['data'] = $data_json;
		echo json_encode($result);
	}

	public function add()
	{
		$now = $this->get_date_now();
		$data['id'] = $this->get_unix_id();
		$data['kategori'] = $this->input->post('kategori');
		$data['ctd'] = $now;
		$data['mdd'] = $now;

		$result = $this->{$this->base_model}->add_data($data, $this->base_table);
		redirect('kategori');
	}

	public function edit()
	{
		$data['id'] = $this->input->post('id');
		$result = $this->{$this->base_model}->get_data_by_id($data, $this->base_table);
		echo json_encode($result);
	}

	public function store_edit()
	{
		$id = $this->input->post('id');
		$data['kategori'] = $this->input->post('kategori');
		$result = $this->{$this->base_model}->update_data_by_id($id, $data, $this->base_table);
	}

	public function delete()
	{
		$data['id'] = $this->input->post('data_id');
		$result = $this->{$this->base_model}->delete_data_by($data, $this->base_table);
		echo json_encode($result);
	}
}
