<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section>
  <div class="row text-right">
    <div class="col-12 col-md-12">
      <button type="button" class="btn btn-md btn-primary btn-icon-text" data-toggle="modal" data-target="#formModal">
        <i class="fas fa-plus"></i> Add
      </button>
      <!-- Modal -->
      <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="formModalTitle">Tambah Kategori Sapi</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form_add" method="POST" class="forms-sample">
                <div class="form-group text-left">
                  <label for="kategori">Category</label>
                  <input type="text" class="form-control" id="kategori" name="kategori" placeholder="Kategori Sapi">
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="btn_close" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
            </div>
          </div>
        </div>
      </div>
      <?php $this->load->view('modal_edit'); ?>
      <button type="button" id="btn_rld" class="btn btn-md btn-success btn-icon-text">
        <i class="fas fa-sync"></i> Refresh
      </button>
    </div>
  </div>
  <hr>
</section>
<section>
  <div class="row mt-4">
    <div class="col-lg-12 grid-margin">
      <div class="row">
        <div class="table">
          <table class="table table-hover bg-aqua" id="dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Category</th>
                <!-- <th>Status</th> -->
                <th>Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>