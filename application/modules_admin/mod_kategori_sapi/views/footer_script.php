<script type="text/javascript">
  var base_url = "<?= base_url() ?>" + "mod_kategori_sapi/";
  var table = null;
  $(document).ready(function() {
    var table = $('#dataTable').DataTable({
      // "autoWidth": true,
      "processing": true,
      "searchDelay": 500,
      // "serverSide": true,
      "ordering": true,
      "ajax": {
        "url": base_url + "table",
        "type": "POST"
      },
      'columnDefs': [{
          'targets': 0,
          'orderable': true,
          'width': '10%'
        },
        {
          'targets': 1,
          'width': '60%'
        },
        {
          'targets': 2,
          'width': '20%'
        }
      ]
    });

    $('#dataTable tbody').on('click', 'td #btn_del', function() {
      var id = $(this).attr('data-id');
      swal({
          title: "Hapus data",
          text: "Apakah anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: "POST",
              url: base_url + "delete",
              data: {
                data_id: id
              },
              dataType: "json",
              success: function(res) {
                if (res == true) {
                  swal('berhasil', {
                    icon: "success",
                  });
                  table.ajax.reload();
                } else {
                  swal('Gagal', {
                    icon: "error",
                  });
                }
              }
            });
          } else {
            swal("Hapus data dibatalkan!");
          }
        });
    });

    $('#btn_save').click(function(e) {
      var data = $('#form_add').serialize();
      $.ajax({
        type: "POST",
        url: base_url + "add",
        data: data,
        success: function(response) {
          table.ajax.reload();
          $('#form_add #kategori').val('');
          $('#btn_close').click();
        }
      });
    });

    $('#btn_rld').click(function(e) {
      table.ajax.reload();
    });

    $('#dataTable tbody').on('click', 'td #btn_edit', function() {
      var id = $(this).attr('data-id');
      $.ajax({
        type: "POST",
        url: base_url + "edit",
        data: {
          id: id
        },
        dataType: "JSON",
        success: function(res) {
          $('#formEditModal #ketegori').val(res.kategori);
          $('#formEditModal #id').val(res.id);
        }
      });
    });

    $('#btn_save_edit').click(function(e) {
      var data = $('#form_edit').serialize();
      $.ajax({
        type: "POST",
        url: base_url + "store_edit",
        data: data,
        success: function(res) {
          table.ajax.reload();
          $('.modal-footer #btn_close').click();
        }
      });
    });
  });
</script>