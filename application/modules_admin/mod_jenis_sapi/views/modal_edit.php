<div class="modal fade" id="formEditModal" tabindex="-1" role="dialog" aria-labelledby="formEditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formEditModalTitle">Edit Jenis Sapi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_edit" method="POST" class="forms-sample">
          <div class="form-group text-left">
            <label for="jenis">Jenis Sapi</label>
            <input type="text" class="form-control" id="jenis" name="jenis" placeholder="Jenis Sapi">
            <input type="hidden" class="form-control" id="id" name="id" placeholder="Jenis Sapi">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_close" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" id="btn_save_edit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>