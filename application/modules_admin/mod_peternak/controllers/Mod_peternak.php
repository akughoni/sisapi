<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/Base_admin.php');

class Mod_peternak extends Base_admin
{
	protected $base_model = "peternak_model";
	protected $base_table = "peternak";

	protected $page = [
		'index' => 'index',
		'footer_script' => 'footer_script',
	];
	private $title_page = "PETERNAK";
	private $primary_key = "id";
	private $field_get = array(
		'id', 'nama', 'alamat', 'nama_kelurahan', 'umur', 'pendidikan'
	);
	private $field_add = array(
		'id', 'nama', 'alamat', 'kelurahan_id', 'umur', 'pendidikan'
	);

	function __construct()
	{
		parent::__construct();
		$this->load->model($this->base_model);
	}

	public function index()
	{
		$data['title_page'] = $this->title_page;
		$this->display($this->page['index'], $data, $this->page['footer_script']);
	}

	public function table()
	{
		$value = [];
		$data_json = [];
		$data = $this->{$this->base_model}->get_data_peternak($this->base_table);

		foreach ($data as $index => $col) {
			foreach ($this->field_get as $key => $get) {				
				$value[] = $col[$get];
			}

			$data_button = [
				'id' => $col[$this->primary_key]
			];

			//load button
			$button_action = $this->load->view('button', $data_button, true);
			$value[] = $button_action;
			$data_json[$index] = $value;
			$value = [];
		}
		$result['data'] = $data_json;
		echo json_encode($result);
	}

	public function add()
	{
		$now = $this->get_date_now(); //get date now
		$data = [];

		//set data from post to data by field properties
		foreach($this->field_add as $value){
			$data[$value] = $this->input->post($value);
		}
		
		$data['ctd'] = $now;
		$data['mdd'] = $now;
		$result = $this->{$this->base_model}->add_data($data, $this->base_table);
		return $result;
	}

	public function edit()
	{
		$data['id'] = $this->input->post('id');
		$result = $this->{$this->base_model}->get_data_by_id($data, $this->base_table);
		echo json_encode($result);
	}

	public function store_edit()
	{
		$now = $this->get_date_now(); //get date now
		$data = [];
		//set data from post to data by field properties
		foreach ($this->field_add as $value) {
			$data[$value] = $this->input->post($value);
		}
		$data['mdd'] = $now;
		$id = $this->input->post($this->primary_key);
		$result = $this->{$this->base_model}->update_data_by_id($id, $data, $this->base_table);
		return $result;
	}

	public function delete()
	{
		$data['id'] = $this->input->post('data_id');
		$result = $this->{$this->base_model}->delete_data_by($data, $this->base_table);
		echo json_encode($result);
	}

	public function get_select_option()
	{
		$tabel = $this->input->post('tabel');
		$this->load->model('admin_model');
		$result = $this->admin_model->get_all_data($tabel);

		echo json_encode($result);
	}
}
