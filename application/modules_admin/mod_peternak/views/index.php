<section class="col-12">
  <h2 class=""><i class="fas fa-angle-right"></i> <?= $title_page ?></h2>
</section>
<section>
  <div class="row text-right">
    <div class="col-12 col-md-12">
      <button type="button" class="btn btn-md btn-primary btn-icon-text" id="btn_add" data-toggle="modal" data-target="#formModal">
        <i class="fas fa-plus"></i> Add
      </button>
      <!-- Modal -->
      <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="formModalTitle">Tambah Peternak</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form_add" method="POST" class="forms-sample">
                <div class="form-group text-left">
                  <label for="id">ID</label>
                  <input type="text" class="form-control" id="id" name="id" placeholder="ID">
                </div>
                <div class="form-group text-left">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                </div>
                <div class="form-group text-left">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="2"></textarea>
                </div>
                <div class="form-group text-left">
                  <label for="kelurahan_id">Kelurahan</label>
                  <select class="browser-default custom-select" name="kelurahan_id" id="kelurahan_id">
                    <option>Pilih Kelurahan</option>
                  </select>
                </div>
                <div class="form-group text-left">
                  <label for="umur">Umur</label>
                  <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur">
                </div>
                <div class="form-group text-left">
                  <label for="pendidikan">Pendidikan</label>
                  <select class="browser-default custom-select" name="pendidikan" id="pendidikan">
                    <option value="Tidak Tamat SD">Tidak Tamat Sekolah</option>
                    <option value="SD">SD</option>
                    <option value="SLTP">SLTP</option>
                    <option value="SLTA">SLTA</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="btn_close" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
            </div>
          </div>
        </div>
      </div>
      <?php $this->load->view('modal_edit'); ?>
      <button type="button" id="btn_rld" class="btn btn-md btn-success btn-icon-text">
        <i class="fas fa-sync"></i> Refresh
      </button>
    </div>
  </div>
  <hr>
</section>
<section>
  <div class="row mt-4">
    <div class="col-lg-12 grid-margin">
      <div class="row">
        <div class="table">
          <table class="table table-hover bg-aqua" id="dataTable">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Kelurahan</th>
                <th>Umur</th>
                <th>Pendidikan</th>
                <th>Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>