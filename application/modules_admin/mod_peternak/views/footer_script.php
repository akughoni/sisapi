<script type="text/javascript">
  var base_url = "<?= base_url() ?>" + "mod_peternak/";
  var table = null;
  // var name_field = "nama_kelurahan";
  $(document).ready(function() {
    //Get DataTables
    var table = $('#dataTable').DataTable({
      "autoWidth": true,
      "processing": true,
      "searchDelay": 500,
      // "serverSide": true,
      "ordering": true,
      "ajax": {
        "url": base_url + "table",
        "type": "POST"
      },
      // 'columnDefs': 
    });
    //Get Data For Selectboc Option Kelurahan
    function set_option() {
      $.ajax({
        type: "POST",
        url: base_url + 'get_select_option',
        data: {
          tabel: 'kelurahan'
        },
        dataType: "JSON",
        success: function(res) {
          res.forEach(value => {
            // console.log(el);
            var option = "<option value='" + value.id + "'>" + value.nama_kelurahan + "</option>";
            $('#form_add #kelurahan_id').append(option);
            $('#form_edit #kelurahan_id').append(option);
          });
        }
      });
    }

    $('#btn_add').on('click', set_option());
    //Delete Data by Button on action
    $('#dataTable tbody').on('click', 'td #btn_del', function() {
      var id = $(this).attr('data-id');
      swal({
          title: "Hapus data",
          text: "Apakah anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              type: "POST",
              url: base_url + "delete",
              data: {
                data_id: id
              },
              dataType: "json",
              success: function(res) {
                if (res == true) {
                  swal('berhasil', {
                    icon: "success",
                  });
                  table.ajax.reload();
                } else {
                  swal('Gagal', {
                    icon: "error",
                  });
                }
              }
            });
          } else {
            swal("Hapus data dibatalkan!");
          }
        });
    });
    //Add data on modal form
    $('#btn_save').click(function(e) {
      var data = $('#form_add').serialize();
      console.log(data);
      $.ajax({
        type: "POST",
        url: base_url + "add",
        data: data,
        success: function(response) {
          table.ajax.reload();
          $('#btn_close').click();
        }
      });
    });
    //reload datatables
    $('#btn_rld').click(function(e) {
      table.ajax.reload();
    });
    //Get data for form modal edit data
    $('#dataTable tbody').on('click', 'td #btn_edit', function() {
      var id = $(this).attr('data-id');
      $.ajax({
        type: "POST",
        url: base_url + "edit",
        data: {
          id: id
        },
        dataType: "JSON",
        success: function(res) {
          $('#formEditModal #nama').val(res.nama);
          $('#formEditModal #alamat').val(res.alamat);
          $('#formEditModal #umur').val(res.umur);
          $('#formEditModal #id').val(res.id);
          // set_option();
          $('#formEditModal #kelurahan_id').val(res.kelurahan_id);
          $('#formEditModal #pendidikan').val(res.pendidikan);
        }
      });
    });
    //changes data proses after edit data on save
    $('#btn_save_edit').click(function(e) {
      var data = $('#form_edit').serialize();
      $.ajax({
        type: "POST",
        url: base_url + "store_edit",
        data: data,
        success: function(res) {
          table.ajax.reload();
          $('.modal-footer #btn_close').click();
        }
      });
    });
  });
</script>