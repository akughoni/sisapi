<div class="modal fade" id="formEditModal" tabindex="-1" role="dialog" aria-labelledby="formEditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formEditModalTitle">Edit Peternak</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_edit" method="POST" class="forms-sample">
          <div class="form-group text-left">
            <label for="id">ID</label>
            <input type="text" class="form-control" id="id" name="id" placeholder="ID">
          </div>
          <div class="form-group text-left">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
          </div>
          <div class="form-group text-left">
            <label for="alamat">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="2"></textarea>
          </div>
          <div class="form-group text-left">
            <label for="kelurahan_id">Kelurahan</label>
            <select class="browser-default custom-select" name="kelurahan_id" id="kelurahan_id">
              <option>Pilih Kelurahan</option>
            </select>
          </div>
          <div class="form-group text-left">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur">
          </div>
          <div class="form-group text-left">
            <label for="pendidikan">Pendidikan</label>
            <select class="browser-default custom-select" name="pendidikan" id="pendidikan">
              <option value="SD">SD</option>
              <option value="SLTP">SLTP</option>
              <option value="SLTA">SLTA</option>
              <option value="S1">S1</option>
              <option value="S2">S2</option>
              <option value="S3">S3</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_close" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" id="btn_save_edit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>