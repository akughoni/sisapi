<div class="container-fluid py-5">
  <div class="row justify-content-center py-5">
    <div class="col-12 col-sm-10 col-md-4">
      <form class="text-center shadow-lg rgba-white-strong p-5" method="post" action="<?php base_url('auth/login') ?>">
        <p class="h4 mb-4">Sign in Administrator</p>
        <input type="text" id="identity" name="identity" class="form-control mb-4" placeholder="E-mail">
        <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">
        <div class="d-flex justify-content-around">
          <div class="col-12"
            <!-- Default unchecked -->
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="remember">
              <label class="custom-control-label" for="remember" name="remember" value="1">Remember</label>
            </div>
          </div>
          <!-- <div>
            <a href="">Forgot password?</a>
          </div> -->
        </div>
        <button class="btn rgba-teal-strong btn-block my-4" type="submit">Sign in</button>
      </form>
    </div>
  </div>
</div>