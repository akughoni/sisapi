<section class="col-12">
	<h2 class=""><i class="fas fa-angle-right"></i><?php echo lang('create_user_heading'); ?></h2>
	<p><?php echo lang('create_user_subheading'); ?></p>

</section>
<hr>
<section class="row justify-content-center mt-3">
	<div class="col-10 border-left border-right">
		<div id="infoMessage"><?php echo $message; ?></div>
		<?php echo form_open("auth/create_user"); ?>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_fname_label', 'first_name'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($first_name); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_lname_label', 'last_name'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($last_name); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('create_user_email_label', 'email'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($email); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_company_label', 'company'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($company); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_phone_label', 'phone'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($phone); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_password_label', 'password'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($password); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="inputEmail3MD" class="col-sm-3 col-form-label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm'); ?></label>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<?php echo form_input($password_confirm); ?>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<div class="md-form mt-0">
					<!-- <input type="email" class="form-control" id="inputEmail3MD" placeholder="Email"> -->
					<a href="<?= base_url('auth') ?>" class="btn btn-outline-primary">Cancel</a>
					<?php echo form_submit('submit', lang('edit_user_submit_btn'), "class='btn btn-primary'"); ?>
				</div>
			</div>
		</div>

		<?php echo form_close(); ?>
	</div>
</section>