<script>
  $(document).ready(function() {
    $('.table tbody tr td #btn_del_user').click(function(e) {
      swal("Apakah anda yakin menghapus user ini?", {
          buttons: {
            cancel: "cancel",
            yakin: "Yakin!",
          },
        })
        .then((value) => {
          switch (value) {
            case "yakin":
              var link = $(this).attr('data-link');
              // console.log(link);
              delete_user(link);
              // swal("Berhasil", "hapus akun!", "success");
              break;
            default:
              swal("Hapus akun dibatalkan!");
          }
        });
    });

    function delete_user(link) {
      $.ajax({
        type: "GET",
        url: link,
        success: function(response) {
          if(response){
            swal("Berhasil", "hapus akun!", "success").then(()=> {
              location.reload();
            });
          }
        }
      });
    }

  });
</script>