<section class="col-12">
	<h2 class=""><i class="fas fa-angle-right"></i><?php echo lang('index_heading'); ?></h2>
</section>
<section>
	<div class="row text-right">
		<div class="col-12 col-md-12">
			<a class="btn btn-md btn-primary btn-icon-text" id="btn_add" href="<?= base_url('auth/create_user') ?>">
				<i class="fas fa-plus"></i> Add
			</a>
			<button type="button" id="btn_rld" class="btn btn-md btn-success btn-icon-text">
				<i class="fas fa-sync"></i> Refresh
			</button>
		</div>
	</div>
	<hr>
</section>
<section>
	<!-- <div class="row mt-4"> -->
	<!-- <div class="col-md-12"> -->
	<div class="table">
		<table class="table table-hover bg-aqua text-center">
			<thead>
				<tr>
					<th><?php echo lang('index_fname_th'); ?></th>
					<th><?php echo lang('index_lname_th'); ?></th>
					<th><?php echo lang('index_email_th'); ?></th>
					<th><?php echo lang('index_groups_th'); ?></th>
					<th><?php echo lang('index_status_th'); ?></th>
					<th><?php echo lang('index_action_th'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user) : ?>
					<tr>
						<td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
						<td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
						<td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
						<td>
							<?php foreach ($user->groups as $group) : ?>
								<p><?= $group->name ?></p>
								<?php //echo anchor("auth/edit_group/" . $group->id, htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8')); 
								?>
							<?php endforeach ?>
						</td>
						<td><?php echo ($user->active) ? anchor("auth/deactivate/" . $user->id, lang('index_active_link')) : anchor("auth/activate/" . $user->id, lang('index_inactive_link')); ?></td>
						<td>
							<?php echo anchor("auth/edit_user/" . $user->id, '<i class="fas fa-edit"></i>'); ?>
							<?php //echo anchor("", '<i class="fas fa-trash"></i>', "id='btn_del_user'"); ?>
							<a id='btn_del_user' data-link="<?= "auth/delete_user/" . $user->id ?>"><i class="fas fa-trash"></i></a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<!-- </div>
    </div> -->
	</div>
</section>